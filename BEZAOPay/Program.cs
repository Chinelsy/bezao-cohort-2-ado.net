﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using System.Configuration;
using BEZAOPayDAL.Models;


namespace BEZAOPay
{
    class Program
    { 
        static void Main(string[] args)
        {
            var connectionstring = ConfigurationManager.ConnectionStrings["BezaoConnect"].ConnectionString;
            var db = new BEZAODAL();
            
            var users = db.GetAllUsers();
            foreach (var use in users)
            {
               Console.WriteLine($"Id: {use.Id}\nName: {use.Name}\nEmail: {use.Email}");

            }
            var user = new User
             {
                 Name = "Egonwa",
                 Email = "nwa@domain.com"
             };

             db.CreateUser(user);
             db.CreateUser("Lucy", "Lucy@domain.com");
             var account = new Account
             {
                 UserId = 1006,
                 AccountNumber = 1234567890,
                 Balance = (decimal)14000000.98

             };

             db.CreateAccount(account);

            // for store procedures
            db.InsertUserProcedure("Nneka", "nk@domain.com");
            db.InsertAccountProcedure(1007, 1234567899, 15000000.99m);

            // Delection Logic 
            db.DeleteUser(6000);

            //Update logic
            db.UpdateUser(new BEZAOPayDAL.Models.Account { Id = 13, Balance = 200000.89m }); 
            db.SendMoney(new BEZAOPayDAL.Models.Account { Id = 16, Balance = 1034670.99m},
            new BEZAOPayDAL.Models.Transaction { UserId = 16 , Mode = "Debit", Amount = 2000.00m, Time = DateTime.Now });
            Console.WriteLine("Transaction was successful");
            Console.ReadKey();
        }
        
        
    }

}
